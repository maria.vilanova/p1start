package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;
import edu.uprm.cse.datastructures.cardealer.model.Car;

public class CarComparator implements Comparator<Car> {
	
	/*
	 * This Comparator class is used to compare two Car objects and determine their sorting order.
	 */

	@Override
	public int compare(Car c1, Car c2) {
		/*
		 * This method takes two Car objects (c1, c2) as parameters, compares their brand, model and model option lexicographically, 
		 * and determines their relative order. 
		 * It returns zero if the objects are equal, a negative value if c1 precedes c2, or a positive value if c1 follows c2.
		 */
		
		if(c1.getCarBrand().equals(c2.getCarBrand())) {
			if(c1.getCarModel().equals(c2.getCarModel())) {
				return c1.getCarModelOption().compareTo(c2.getCarModelOption());
			}
			return c1.getCarModel().compareTo(c2.getCarModel());
		}

		return c1.getCarBrand().compareTo(c2.getCarBrand());

	}

}
