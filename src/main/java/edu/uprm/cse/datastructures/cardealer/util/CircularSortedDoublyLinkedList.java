package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

import edu.uprm.cse.datastructures.cardealer.model.CarComparator;

public class CircularSortedDoublyLinkedList<E> implements SortedList<E>{
	/*
	 * This class implements the SortedList ADT as a circular doubly-linked list with dummy header, which sorts its elements in
	 * relatively increasing order using a parameter Comparator instance.
	 * If the Comparator is not specified, it uses a Regular Comparator instance instead, by default.
	 */

	private class CircularSortedDoublyLinkedListIterator implements Iterator<E>{
		/*
		 * This is the inner Iterator class for the CircularSortedDoublyLinkedList data structure, in the "forward" direction.
		 */

		private Node<E> nextNode;


		public CircularSortedDoublyLinkedListIterator() {
			this.nextNode = (Node<E>) header.getNext();
		}

		@Override
		public boolean hasNext() {
			return nextNode != header;
		}

		@Override
		public E next() {
			if (this.hasNext()) {
				E result = this.nextNode.getElement();
				this.nextNode = this.nextNode.getNext();
				return result;
			}
			else {
				throw new NoSuchElementException();
			}
		}
	}		

	private static class Node<E>{
		/*
		 * This is the private inner Node class for the CircularSortedDoublyLinkedList data structure.
		 */

		private E element;
		private Node<E> next;
		private Node<E> previous;

		public Node() {
			this.element = null;
			this.next = this.previous = null;

		}
		public E getElement() {
			return element;
		}
		public void setElement(E element) {
			this.element = element;
		}
		public Node<E> getNext() {
			return next;
		}
		public void setNext(Node<E> next) {
			this.next = next;
		}
		public Node<E> getPrevious() {
			return previous;
		}
		public void setPrevious(Node<E> previous) {
			this.previous = previous;
		}

	}

	public class RegularComparator implements Comparator<E> {
		/*
		 * This is the default comparator class for the CircularSortedDoublyLinkedList data structure.
		 * It compares two objects for relative order according to their string representation.
		 */

		@Override
		public int compare(E o1, E o2) {
			/*
			 * This method compares two objects (o1, o2) according to their string representation, and determines their relative order.
			 * It returns zero if the objects are equal, a negative value of o1 precedes o2, or a positive value if o1 follows o2.
			 */
			
			return o1.toString().compareTo(o2.toString());
		}

	}

	public Node<E> header;
	public int currentSize;
	public Comparator<E> comparator;

	public CircularSortedDoublyLinkedList(){

		this.currentSize = 0;
		this.header = new Node<E>();
		this.header.setNext(this.header);
		this.header.setPrevious(this.header);
		this.comparator = new RegularComparator();

	}

	public CircularSortedDoublyLinkedList(Comparator<E> comp){

		this();
		this.comparator = comp;

	}

	@Override
	public Iterator<E> iterator() {
		/*
		 * This method generates and returns an Iterator for the list.
		 */
		
		return new CircularSortedDoublyLinkedListIterator();
	}

	@Override
	public boolean add(E obj) {
		/*
		 * This method adds a specified object to the list, in the order determined by the list's comparator. 
		 * The method returns true if the parameter object is successfully added to the list, and false otherwise.
		 * (Note: The method will return false if the parameter object is null.)
		 */
		
		//if(obj == null || this.contains(obj)) {
		if(obj.equals(null)) {
			return false;
		}

		Node<E> newNode = new Node<E>();
		newNode.setElement(obj);

		if(this.isEmpty()) {
			this.header.setNext(newNode);
			this.header.setPrevious(newNode);
			newNode.setNext(this.header);
			newNode.setPrevious(this.header);
			this.currentSize++;
			return true;
		}

		else {

			Node<E> temp = this.header.getNext();
			while(temp != this.header){
				if(comparator.compare(temp.getElement(), obj) >= 0) { 
					newNode.setNext(temp);
					newNode.setPrevious(temp.getPrevious());
					temp.getPrevious().setNext(newNode);
					temp.setPrevious(newNode);
					this.currentSize++;
					return true;
				}
				temp = temp.getNext();
			}

			newNode.setNext(this.header);
			newNode.setPrevious(this.header.getPrevious());
			this.header.getPrevious().setNext(newNode);
			this.header.setPrevious(newNode);
			this.currentSize++;
			return true;

		}

	}

	@Override
	public int size() {
		/*
		 * This method returns the list's current size.
		 */
		
		return this.currentSize;
	}

	@Override
	public boolean remove(E obj) {
		/*
		 * This method removes the first instance of a specified object from the list.
		 * It returns true if the parameter object was removed successfully, or false otherwise.
		 * (Note: The method returns false if the parameter object is not present in the list.)
		 */
		
		if(this.contains(obj)) {
			int index = this.firstIndex(obj);
			return this.remove(index);
		}
		return false;

	}

	@Override
	public boolean remove(int index) {
		/*
		 * This method removes the list element in the position specified by a parameter index.
		 * It returns true if the element was successfully removed, or false otherwise. 
		 */
		
		if((index < 0) || (index >= this.currentSize)) {
			//	return false;
			throw new IndexOutOfBoundsException();
		}
		Node<E> temp = this.getNode(index);
		temp.getPrevious().setNext(temp.getNext());
		temp.getNext().setPrevious(temp.getPrevious());
		temp.setNext(null);
		temp.setPrevious(null);
		temp.setElement(null);
		this.currentSize--;
		return true;
	}

	@Override
	public int removeAll(E obj) {
		/*
		 * This method removes all instances of the parameter object from the list. 
		 * It returns the total number of instances removed.
		 */
		
		int result = 0;
		while(this.contains(obj)) {
			this.remove(obj);
			result++;
		}

		return result;
	}

	@Override
	public E first() {
		/*
		 * This method returns the first element in the list (in the "forward" direction), or null if the list is empty.
		 */
		
		return (this.isEmpty()) ? null : this.header.getNext().getElement();
	}

	@Override
	public E last() {
		/*
		 * This method returns the last element in the list (in the "forward" direction), or null if the list is empty.
		 */
		
		return (this.isEmpty()) ? null : this.header.getPrevious().getElement();
	}

	@Override
	public E get(int index) {
		/*
		 * This method finds and returns the list element in the position specified by a parameter index.
		 * It returns null if the element is not found. 
		 */
		
		if((index < 0) || (index >= this.currentSize)) {
			throw new IndexOutOfBoundsException();
		}
		return this.getNode(index).getElement();
	}

	@Override
	public void clear() {
		/*
		 * This method clears the list of all elements, reverting it to its original state.
		 */
		
		while(!this.isEmpty()) {
			this.remove(0);
		}
	}

	@Override
	public boolean contains(E e) {
		/*
		 * This method determines if the list contains a specified element.
		 * It returns true if the parameter object (e) is present somewhere in the list, and false otherwise.
		 */
		
		return this.firstIndex(e) >= 0;
	}

	@Override
	public boolean isEmpty() {
		/*
		 * This method determines if the list is empty.
		 * It returns true if the list is empty, and false otherwise.
		 */
		
		return this.size() == 0;
	}

	@Override
	public int firstIndex(E e) {
		/*
		 * This method determines the position of the first appearance of a parameter object (e) in the list. 
		 * It returns the index of the first instance of the specified object, or -1 if it is not found.
		 */
		
		int index = 0;
		for(Node<E> temp = this.header.getNext(); temp != this.header; temp = temp.getNext(), index++) {
			if(temp.getElement().equals(e)) {
				return index;
			}
		}
		return -1;
	}

	@Override
	public int lastIndex(E e) {
		/*
		 * This method determines the position of the last appearance of a parameter object (e) in the list. 
		 * It returns the index of the last instance of the specified object, or -1 if it is not found.
		 */
		
		int index = this.currentSize - 1;
		for(Node<E> temp = this.header.getPrevious(); temp != this.header; temp = temp.getPrevious(), index--) {
			if(temp.getElement().equals(e)) {
				return index;
			}
		}
		return -1;
	}

	private Node<E> getNode(int index){
		/*
		 * This private method returns the node in the position specified by a parameter index. 
		 * It returns null if the list is empty.
		 */
		
		int position = 0;
		Node<E> temp = this.header.getNext();

		if(temp.equals(this.header)) {
			return null;
		}

		while(position != index) {
			temp = temp.getNext();
			position++;
		}

		return temp;
	}

}
