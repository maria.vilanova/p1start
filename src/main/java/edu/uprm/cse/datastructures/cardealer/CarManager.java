package edu.uprm.cse.datastructures.cardealer;

import javax.ws.rs.Path;

import java.util.Iterator;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarList;
import edu.uprm.cse.datastructures.cardealer.util.SortedList;

@Path("/cars")
public class CarManager {
	/*
	 * This class defines the REST API for the car dealer server.
	 * It contains CRUD methods, for adding, getting, updating and removing Car objects from the server's car list.
	 */

	private final SortedList<Car> singleton = CarList.getInstance();

	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car car){
		/*
		 * This method adds a new Car object to the car list. 
		 * It returns an HTTP 201 (Created) response if the parameter Car is successfully added to the collection.
		 * It returns an HTTP 400 (Bad Request) response if the Car was not successfully added,
		 * or if there was already another Car element with the same ID in the list.
		 * (Note: The list must contain Car objects with unique IDs.)
		 */

		for(Car temp : singleton) {
			if(temp.getCarId() == car.getCarId()) {
				return Response.status(Response.Status.BAD_REQUEST).build();
			}
		}

		if(singleton.add(car)) {
			return Response.status(201).build();
		}
		
		return Response.status(Response.Status.BAD_REQUEST).build();
	}

	@GET
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() {
		/*
		 * This method retrieves the list of all cars from the server.
		 * It returns an array of Car objects, containing all the cars present in the collection.
		 */
		
		Car[] allCars = new Car[singleton.size()];
		int index = 0;
		for(Car temp : singleton) {
			allCars[index] = temp;
			index++;
		}
		return allCars;
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id){
		/*
		 * This method retrieves a specific car from the server.
		 * It returns the Car object with the specified ID from the car list.
		 * (Note: If the car object is not found, the method will throw a Not Found exception.)
		 */
		
		for(Car temp : singleton) {
			if(temp.getCarId() == id) {
				return temp;
			}
		}

		throw new NotFoundException();
	}

	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(Car car){
		/*
		 * This method updates an existing Car object from the car list. 
		 * It returns an HTTP 200 (OK) response if the parameter Car is found and successfully updated in the collection.
		 * It returns an HTTP 404 (Not Found) response if the Car is not found.
		 */
		
		long targetId = car.getCarId();

		for(Car temp : singleton) {
			if(temp.getCarId() == targetId) {
				if(singleton.remove(temp) && singleton.add(car)) {
					return Response.status(Response.Status.OK).build();
				}
			}
		}

		return Response.status(Response.Status.NOT_FOUND).build();
	}


	@DELETE
	@Path("/{id}/delete")
	public Response deleteCar(@PathParam("id") long id){
		/*
		 * This method deletes a specific Car object from the car list. 
		 * It returns an HTTP 200 (OK) response if the parameter Car is successfully removed from the collection.
		 * It returns an HTTP 404 (Not Found) response if the Car is not found or was not successfully removed.
		 */

		for(Car temp : singleton) {
			if(temp.getCarId() == id) {
				if(singleton.remove(temp)) {
					return Response.status(Response.Status.OK).build();
				}
			}
		}

		return Response.status(Response.Status.NOT_FOUND).build();
	}



}
