package edu.uprm.cse.datastructures.cardealer.model;

import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;
import edu.uprm.cse.datastructures.cardealer.util.SortedList;

public class CarList {
	/*
	 * This class is used to generate and export an instance of the list data structure to be used by the car dealer server
	 * (CircularSortedDoublyLinkedList). 
	 * It uses the getInstance() getter method to make the list available to other classes.
	 */
	
	// A variable to store the reference to the current list data structure instance in use
	private static SortedList<Car> carList = new CircularSortedDoublyLinkedList<>(new CarComparator());;
	
	private CarList() {}

	public static SortedList<Car> getInstance(){
		/*
		 * This method returns a list data structure instance (carList).
		 */
		
		return carList;
	}
	
	public static void resetCars() {
		/*
		 * This method resets the list instance (carList) to its original state.
		 */
		
		carList = new CircularSortedDoublyLinkedList<>(new CarComparator());
	}

}
